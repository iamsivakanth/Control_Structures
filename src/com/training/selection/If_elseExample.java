package com.training.selection;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class If_elseExample {
	void display() {
		LocalDate date = LocalDate.now();
		DayOfWeek day = date.getDayOfWeek();

		System.out.println("Date : " + date);
		System.out.println("Month : " + date.getMonth());

		if ("SATURDAY".equalsIgnoreCase(day.toString())) {
			System.out.println("Today is SATURDAY");
		} else {
			System.out.println("Today is not SATURDAY");
		}
	}
}