package com.training.selection;

import java.time.LocalDateTime;
import java.time.LocalTime;

public class DateTimeExample {
	void display() {
		LocalTime time = LocalTime.now();
		System.out.println("Time : " + time);

		LocalDateTime dateTime = LocalDateTime.now();
		System.out.println("Date Time : " + dateTime);
	}
}