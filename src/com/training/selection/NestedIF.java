package com.training.selection;

import java.util.Scanner;

public class NestedIF {

	final int PIN = 1234;

	void display() {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter your pin : ");
		int pinNumber = scanner.nextInt();
		System.out.print("Enter your gender : ");
		String gender = scanner.next();

		if (pinNumber == PIN) {
			if ("Male".equalsIgnoreCase(gender)) {
				System.out.println("You are BOY");
			} else if ("Female".equalsIgnoreCase(gender)) {
				System.out.println("You are GIRL");
			} else {
				System.out.println("You are not a BOY and a GIRL");
			}
		} else {
			System.out.println("Invalid PIN entered");
		}
	}
}