package com.training.iterative;

public class SimpleForLoop {

	void display() {
		for (int i = 1; i <= 10; i++) {
			System.out.println(i);
		}
	}
}