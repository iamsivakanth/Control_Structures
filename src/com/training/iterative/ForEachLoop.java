package com.training.iterative;

public class ForEachLoop {
	void display() {
		int[] arr = { 1, 2, 3, 4, 7, 5, 6, 9, 8 };
//		for (int i = 0; i < arr.length; i++) {
//			System.out.println(arr[i]);
//		}

		for (int a : arr) {
			System.out.println(a);
		}
	}
}