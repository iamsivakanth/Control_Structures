package com.training.iterative;

public class WhileLoop {

	void display() {
		int a = 1;
		while (a <= 5) {
			System.out.println(a);
			a++;
		}
	}
}