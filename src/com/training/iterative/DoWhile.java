package com.training.iterative;

import java.util.Scanner;

public class DoWhile {

	void display() {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter starting number : ");
		int number = scanner.nextInt();
		
		do {
			System.out.println(number);
			number++;
		} while (number <= 5);
	}
}